import cheerio from 'cheerio'
import unirest from 'unirest'
import bluebird from 'bluebird'
import _ from 'lodash'
import querystring from 'querystring'

const API_URI = 'https://app.instapage.com'
const CookieJar = unirest.jar();
const Promise = bluebird;

export class Instapage {
  constructor(credentials) {
    this.email = credentials.email;
    this.password = credentials.password;
  }
  

  // init instapage session, store cookie for future request
  init() {
    const actionUri = 'auth/login';
    const credentials = {
      'email': this.email,
      'password': this.password,
      'csrf-token': null,
      'uri': ''
    };

    return new Promise((resolve, reject) => {
      unirest.get(API_URI + '/' + actionUri).jar(CookieJar).end((response) => {
        let $ = cheerio.load(response.body);
        // find csrf for request , every time
        let token  = $('input[name="csrf-token"]').attr('value');
        credentials['csrf-token'] = token;
        unirest.post(API_URI + '/' + actionUri)
          .jar(CookieJar)
          .form(credentials)
          .end(response => {
            resolve(true);
          });
      });
    });
  }
  
  // getting lading pages filter by 'only published'
  getAllPublishedLanding(id) {
    let pages = [];
    return new Promise((resolve, reject) => {
      unirest.get(API_URI + '/' + 'api/1/client/pages/' + id)
      .jar(CookieJar)
      .end(response => {
        _.map(_.filter(response.body.data.pages, {status: 'running'}), p => {
          pages.push(p);
        });
        resolve(pages);
      });
    });
  }
  
  // get user data for getting customer id
  getUserInfo() {
    return new Promise((resolve, reject) => {
      unirest.get(API_URI + '/' + 'ajax/dashboard/get-user-data')
      .jar(CookieJar)
      .end((response) => {
        resolve(JSON.parse(response.body));
      });
    });
  }
  
  // create page
  addPage(name, layout, url) {
    const actionUri = 'builder2';
    let objParam = {
      'layout': layout,
      'page_name': name,
      'csrf-token': ''
    };
    return new Promise((resolve, reject) => {
        unirest.get(API_URI + '/' + 'templates/index/' + objParam.layout + '?skip_preview=1&selected_categories=')
        .jar(CookieJar)
        .end(response => {
          let csrfRegExp = /<input class="csrf-token" type="hidden" name="csrf-token" value="\w+">/g
          let match = csrfRegExp.exec(response.body);
          let $ = cheerio.load(match[0]);
          let token = $('input[name="csrf-token"]').attr('value');
          objParam['csrf-token'] = token;
          unirest.post(API_URI + '/' + actionUri)
            .jar(CookieJar)
            .form(objParam)
            .end(response => {
              let getIdParams = JSON.parse(JSON.stringify(response.request.uri)).query;
              let newPageId = querystring.parse(getIdParams).id;
              this.updatePage(newPageId, url).then(data => {
                resolve(data);
              });
              
            });

        });
    });
  }


  updatePage(id, url) {
    const actionUri = 'ajax/builder2/publish/';
    let objParam = {
      url: url + '.pagedemo.co',
      version: '1'
    }

    return new Promise((resolve, reject) => {
      unirest.post(API_URI + '/' + actionUri + id)
      .jar(CookieJar)
      .form(objParam)
      .end(response => {
        resolve(response.body);
      });
    });
  }

}

