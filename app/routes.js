'use strict';

import { Router } from 'express'
import { Instapage } from './lib/instapage'
import iframeReplacement from 'node-iframe-replacement';
import _ from 'lodash'
const routes = Router();

// init instapage
// set your login, password
const insp = new Instapage(
  {
    email: 'delov.hi@gmail.com', 
    password: '22rrmnyy'
  }
);

routes.get('/api/v1/landing-pages', (req ,res) => {
  insp.init().then(data => {
    insp.getUserInfo().then(data => {
      insp.getAllPublishedLanding(data.data.user['active_customer']).then(data => {
        res.json({status: 'ok', data: { pages: data} });
      });
    });
  }).catch(err => {
    res.status(500).json(err);
  });
});

routes.post('/api/v1/landing-pages', (req, res) => {
  let name = req.body.name;
  let layoutId = req.body.layoutId;
  let url = req.body.url;
  insp.init().then(data => { 
    insp.addPage(name, layoutId, url).then(data => {
      res.json({status: 'ok'});
    });
  });
});

routes.put('/api/v1/landing-pages/:id/', (req , res) => {
  let id = req.params.id;
  let url = req.body.url;
  insp.init().then(data => { 
    insp.updatePage(id, url).then(data => {
      res.json({status: 'ok'});
    });
  });
});

routes.get('/pages/:slug', iframeReplacement,  (req, res) => {
  let slugPage = req.params.slug;
  insp.init().then(data => {
    insp.getUserInfo().then(data => {
      insp.getAllPublishedLanding(data.data.user['active_customer']).then(data => {
        let findPage = _.find(data, p => {
          return p.url.split('.pagedemo.co')[0] == slugPage;
        });
        
        if (findPage && _.has(findPage,'url') && findPage.url !== null) {
          res.merge('index', {
            sourceUrl: 'http://' + findPage.url,
            sourcePlaceholder: 'div[data-entityid="container-top-stories#1"]',
            transform: ( $, model ) => {
              $('title').text(findPage.title);
              $('body').append('<div style="background-color: #2C9F87; right: 0; left: 0; top: 0; position: fixed; z-index: 1030; min-height: 50px; text-align: center;"><h2 style="color: #ffffff;padding-top: 10px;">Instapage NodeJS Application</h2></div>');
            }
          });
        } else {
          res.send('Not available');
        }
      });
    });
  });

  
});

routes.get('/', (req, res) => {
  res.json({name: 'backend_coding_challenge'});
});

export default routes;