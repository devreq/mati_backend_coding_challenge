# Install instruction (tested node v.8.11.3)

## edit login/pass to Instapge 

### 1) app/routes.js

```javascript
const insp = new Instapage(
  {
    email: 'delov.hi@gmail.com', 
    password: '22rrmnyy'
  }
);
```

### 2) npm install
### 3) npm start


# Get all published pages

GET http://localhost:3000/api/v1/landing-pages - get all published pages

# Create Page
POST http://localhost:3000/api/v1/landing-pages

```json
{
	"name": "Test Page",
 	"layoutId": "2119753",
 	"url": "product-1"
}
```

ps. layout id = 2119753 from the challenge example

# Edit slug page

POST http://localhost:3000/api/v1/landing-pages/:id 

```json
{
	"id": "[get from http://localhost:3000/api/v1/landing-pages]",
	"url": "new-slug-product-1"
}
```

# Show landing page through us proxies-server

GET http://localhost:3000/pages/[url page slug]



# About challenge

For reverse engineering used Chrome Network, Restlet Client.
I went to user cases. Authorization -> Page Creation -> Publication, watched requests and tried to reproduce in Restlet. The key point is that you use cookies to store the session and csrf-token in the page creation requests, that you had to retrieve using regexp.
The final substitution was realized through the module node-iframe-replacement.
